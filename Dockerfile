FROM golang

WORKDIR monstache

RUN git clone https://github.com/rwynn/monstache.git
RUN cd monstache && go build .

EXPOSE 8080

CMD ["monstache/monstache"]